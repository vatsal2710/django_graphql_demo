from django.contrib import admin
from movies.models import Actor, Movie


admin.site.register(Actor)
admin.site.register(Movie)

# Register your models here.
