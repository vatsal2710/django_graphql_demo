import graphene
import movies.schema


class Query(movies.schema.Query, graphene.ObjectType):
    # This class will inherit from multiple Queries

    pass


class Mutation(movies.schema.Mutation, graphene.ObjectType):
    # This class will inherit from multiple Queries

    pass


schema = graphene.Schema(query=Query, mutation=Mutation)